# Modules in Java 9

## Main goals of modules
-   __Modular JDK.__ The current jdk is to big it needs to divide into smaller parts which are the modules. 
    Divide the JDK into a set of modules that can be combined at compile time, build time, and run time into a variety of configurations.
-   __Modular source code.__ The current source code files are too big. (rt.jar)


> In Simple Terms, Java has these many First class citizens:
> - In OOP (Object Oriented Programming)  = Object
> - In FP (Functional Programming)        = Function or Lambda
> - In Module System (Modular Programming)= Module



> Java 9 Module Basics
> - Each module has a unique Name.
> - Each module has some description in a source file.
> - A Module description is expressed in a source file called “module-info.java”, it is also known as “Module Descriptor”.
> - A Module Descriptor is a Java file that contains the Module Meta Data.
> - By convention, Module Descriptor file is placed in the top level directory of a Module.
> - Each Module can have any number of Packages and Types.
> - We can create our own modules.
> - One Module can dependent on any number of modules.

> What is Module Meta Data?
> - A unique name.
> - exports clause.
> - requires clause.
> 

> Demonstration
> - library-mod and library-cli without modules
> - library-mod with module and library-cli without module
> - library-mod with module and library-cli with module
> - introducing logging in library-mod

