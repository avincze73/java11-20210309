package hu.sprint;

import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static org.junit.Assert.*;



public class ProcessApiTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void givenCurrentProcess_whenInvokeGetInfo_thenSuccess()
            throws IOException {

        ProcessHandle processHandle = ProcessHandle.current();
        ProcessHandle.Info processInfo = processHandle.info();
        assertNotNull(processHandle.pid());
        assertEquals(true, processInfo.arguments().isPresent());
        assertEquals(true, processInfo.command().isPresent());
        assertTrue(processInfo.command().get().contains("java"));
        assertEquals(true, processInfo.startInstant().isPresent());
        assertEquals(true, processInfo.totalCpuDuration().isPresent());
        assertEquals(true, processInfo.user().isPresent());


    }



    @Test
    public void givenLiveProcesses_whenInvokeGetInfo_thenSuccess() {
        Stream<ProcessHandle> liveProcesses = ProcessHandle.allProcesses();
        liveProcesses.filter(ProcessHandle::isAlive)
                .forEach(ph -> assertNotNull(ph.pid()));
    }


    @Test
    public void givenProcess_whenAddExitCallback_thenSuccess()
            throws Exception {
        ProcessBuilder processBuilder
                = new ProcessBuilder("java", "-version");
        Process process = processBuilder.inheritIO()
                .start();
        ProcessHandle processHandle = process.toHandle();

        System.out.println("PID:  has started " + processHandle.pid());
        CompletableFuture<ProcessHandle> onProcessExit
                = processHandle.onExit();
        onProcessExit.get();
        assertEquals(false, processHandle.isAlive());
        onProcessExit.thenAccept(ph -> {
            System.out.println(("PID: has stopped " + ph.pid()));
        });
    }


}
