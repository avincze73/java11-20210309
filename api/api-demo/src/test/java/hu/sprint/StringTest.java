package hu.sprint;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class StringTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void whenRepeatStringTwice_thenGetStringTwice() {
        //repeats the string content n times
        String output = "La ".repeat(2) + "Land";
        is(output).equals("La La Land");
    }

    @Test
    public void whenStripString_thenReturnStringWithoutWhitespaces() {
        //returns a string with all leading and trailing whitespaces removed
        //stripLeading() and stripTrailing() methods are also added
        is("\n\t  hello   \u2005".strip()).equals("hello");
    }

    @Test
    public void whenTrimAdvanceString_thenReturnStringWithWhitespaces() {
        is("\n\t  hello   \u2005".trim()).equals("hello   \u2005");
    }

    @Test
    public void whenBlankString_thenReturnTrue() {

        //returns true if the string is empty or contains only whitespace
        assertTrue("\n\t\u2005  ".isBlank());
    }

    @Test
    public void whenMultilineString_thenReturnNonEmptyLineCount() {
        //returns a Stream of lines extracted from the string, separated by line terminators
        String multilineStr = "This is\n \n a multiline\n string.";
        long lineCount = multilineStr.lines()
                .filter(String::isBlank)
                .count();

        is(lineCount).equals(3L);
    }
}
