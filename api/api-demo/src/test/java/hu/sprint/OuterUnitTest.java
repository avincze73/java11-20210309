package hu.sprint;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.Test;

public class OuterUnitTest {

    private static final String NEST_HOST_NAME = "hu.sprint.Outer";

    @Test
    public void whenGetNestHostFromOuter_thenGetNestHost() {
        System.out.println(Outer.class.getNestHost().getName());
        assertEquals(Outer.class.getNestHost().getName(), NEST_HOST_NAME);

    }

    @Test
    public void whenGetNestHostFromInner_thenGetNestHost() {
        assertEquals(Outer.Inner.class.getNestHost().getName(), NEST_HOST_NAME);
    }

    @Test
    public void whenCheckNestmatesForNestedClasses_thenGetTrue() {

        assertEquals(Outer.Inner.class.isNestmateOf(Outer.class), true);

    }

    @Test
    public void whenCheckNestmatesForUnrelatedClasses_thenGetFalse() {
        assertNotEquals(Outer.Inner.class.isNestmateOf(Outer.class), false);
    }

    @Test
    public void whenGetNestMembersForNestedClasses_thenGetAllNestedClasses() {
        Set<String> nestMembers = Arrays.stream(Outer.Inner.class.getNestMembers())
                .map(Class::getName)
                .collect(Collectors.toSet());

        assertEquals(nestMembers.size(), 2);

        assertTrue(nestMembers.contains("hu.sprint.Outer"));
        assertTrue(nestMembers.contains("hu.sprint.Outer$Inner"));
    }
}