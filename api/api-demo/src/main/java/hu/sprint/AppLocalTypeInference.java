package hu.sprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class AppLocalTypeInference
{
    public static void main( String[] args )
    {
        var message = "Hello, Java 10";
        Map<Integer, String> map = new HashMap<>();

        var idToNameMap = new HashMap<Integer, String>();

        var empList = new ArrayList<>();

        var obj = new Object() {};


    }
}
