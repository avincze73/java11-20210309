package hu.sprint.java9.privatemethod.interfaces;

public interface BeforeJava8 {
    void f();
    int g(int a);
}
