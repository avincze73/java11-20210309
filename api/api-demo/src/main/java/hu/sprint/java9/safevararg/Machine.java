package hu.sprint.java9.safevararg;

import java.util.ArrayList;
import java.util.List;

public class Machine<T> {
    private List<T> versions = new ArrayList<>();

    @SafeVarargs
    private  void safe(T... toAdd) {
        for (T version : toAdd) {
            versions.add(version);
        }
    }

    @SafeVarargs
    public final void doIt(T... toAdd) {
        safe(toAdd);
    }
}
