package hu.sprint.java9.privatemethod.interfaces;

public interface InJava9 {
    void f();
    int g(int a);
    default void h(){
        setup();
    }
    private void setup(){

    }
    static void m(){
        n();
    }
    private static void n(){

    }
}
