package hu.sprint.java9.diamondoperator;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        MyClass<String> c1 = new MyClass<String>("Gill") {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c1.processData();


        // Allow the diamond operator with anonymous classes
        // if the argument type of the inferred type is denotable

        MyClass<String> c2 = new MyClass<>("Mike") {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c2.processData();

        MyClass<Integer> c3 = new MyClass<>(200) {
            @Override
            void processData() {
                System.out.println("Processing " + getData());
            }
        };
        c3.processData();

        // Cannot infer the type of a non-denotable type (intersection type)
        //   MyClass<T extends Comparable<T> & Serializable> c4 = new MyClass<> {}
    }
}
