package hu.sprint.java9.stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.stream.Collectors.*;
import static java.util.stream.Collectors.toList;

public class StreamApi {

    public static void main(String[] args)  {

        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .takeWhile(integer -> integer < 5)
                .forEach(System.out::println);
        System.out.println("---");

        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .takeWhile(integer -> integer > 5)
                .forEach(System.out::println);
        System.out.println("---");


        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .dropWhile(integer -> integer < 5)
                .forEach(System.out::println);
        System.out.println("---");

        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .dropWhile(integer -> integer > 5)
                .forEach(System.out::println);
        System.out.println("---");

//        Stream<Integer> stream = Stream.iterate(1, i -> i + 1)
//                .filter(x -> x < 4);
//        stream.forEach(System.out::println);

        Stream.iterate(0, i -> i < 10, i -> i + 1)
                .forEach(System.out::println);

        Map<String, String> nickNames = new HashMap<>();
        nickNames.put("Frank", "Franky");
        nickNames.put("James", "JC");
        nickNames.put("Oscar", null);
        nickNames.put("Michael", "Mike");
        nickNames.put("Herb", "Herbie");
        nickNames.put("Elizabeth", "Liz");
        nickNames.put("Suzy", null);

        List<String> names = nickNames.entrySet()
                .stream()
                .map(e -> e.getKey())
                .collect(toList());
        System.out.println(names);

        List<String> nick1 = nickNames.entrySet()
                .stream()
                .flatMap(e -> {
                    if (e.getValue() != null) {
                        return Stream.of(e.getValue());
                    } else {
                        return Stream.empty();
                    }
                })
                .collect(toList());

        System.out.println(nick1);

        List<String> nick2 = nickNames.entrySet()
                .stream()
                .flatMap(e -> Stream.ofNullable(e.getValue()))
                .collect(toList());
        System.out.println(nick2);
    }
}
