package hu.sprint.java9.process;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

public class ProcessApi {

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        ProcessHandle ph = ProcessHandle.current();
        long PID = ph.pid();
        ProcessHandle.Info procInfo = ph.info();

        Optional<String[]> arg = procInfo.arguments();
        Optional<String> cmd =  procInfo.commandLine();
        Optional<Instant> startTime = procInfo.startInstant();
        Optional<Duration> cpuUsage = procInfo.totalCpuDuration();


//        ProcessBuilder processBuilder = new ProcessBuilder("java", "-version");
//        Process process = processBuilder.inheritIO().start();
//        ProcessHandle processHandle = process.toHandle();
//
//
//        Stream<ProcessHandle> liveProcesses = ProcessHandle.allProcesses();
//        liveProcesses.filter(ProcessHandle::isAlive)
//                .forEach(ph2 -> System.out.println(ph2.pid()));
//
//
//        Stream<ProcessHandle> children
//                = ProcessHandle.current().children();
//        children.filter(ProcessHandle::isAlive)
//                .forEach(ph2 -> System.out.println(ph2.pid() + " "  + ph2.info().command()));
//
//        Stream<ProcessHandle> descendants
//                = ProcessHandle.current().descendants();
//        descendants.filter(ProcessHandle::isAlive)
//                .forEach(ph2 -> System.out.println(ph2.pid() + " "  + ph2.info().command()));
//
//
//        ProcessBuilder processBuilder1 = new ProcessBuilder("ls", "-la");
//        processBuilder1.inheritIO();
//        Process process1 = processBuilder1.start();
//        System.out.println(process1.pid());
//        System.out.println("end");
        ProcessBuilder processBuilder2 = new ProcessBuilder("pwd");
        Process process2 = processBuilder2.inheritIO().start();

        ProcessBuilder processBuilder
                = new ProcessBuilder("java", "-version");
        Process process = processBuilder.inheritIO()
                .start();
        ProcessHandle processHandle = process.toHandle();

        System.out.println("PID:  has started " + processHandle.pid());
        CompletableFuture<ProcessHandle> onProcessExit
                = processHandle.onExit();

        onProcessExit.thenAccept(ph3 -> {
            System.out.println(("PID: has stopped " + ph3.pid()));
        });
        onProcessExit.get();
    }
}
