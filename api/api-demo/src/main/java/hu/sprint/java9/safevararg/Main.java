package hu.sprint.java9.safevararg;

import java.util.ArrayList;
import java.util.List;

public class Main {

//     private static <T> T calculateSum(T... args) {
//        T sum = 0;
//        for (int i=0; i < args.length; i++)
//            sum += args[i];
//        return sum;
//    }
    @SafeVarargs
    static <T> void buildList(List<T> list, T... args) {
        for (T item: args)
            list.add(item);
    }

    @SafeVarargs
    private  <T> T[] unsafe(T... args) {
        return args;
    }

    private <T> T[] callUnsafe(T t1, T t2) {
        return unsafe(t1, t2);
    }


    public static void main(String[] args) {

        List<String> l1 = new ArrayList<>();
        buildList(l1, "Mike","Jill");

        List<Integer> l2 = new ArrayList<>();
        buildList(l2, 1,2,3,4,5,6,7,8,9,10);

        //This is the source of the problem
        //String[] result = new Main().callUnsafe("Mike", "Jill");
    }
}
