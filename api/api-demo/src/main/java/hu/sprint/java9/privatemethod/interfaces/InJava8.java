package hu.sprint.java9.privatemethod.interfaces;

public interface InJava8 {
    void f();
    int g(int a);
    default void h(){

    }
}
