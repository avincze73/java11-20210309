package hu.sprint;

import java.lang.reflect.Method;

public class Outer {
    public void outerPublic() {
    }

    private void outerPrivate() {
        System.out.println("outerPrivate");
    }

    class Inner {

        public void innerPublic() {
            outerPrivate();
        }

        public void innerPublicReflection(Outer ob) throws Exception {
            Method method = ob.getClass().getDeclaredMethod("outerPrivate");
            method.invoke(ob);
        }
    }

    public static void main(String[] args) throws Exception {
        Outer outer = new Outer();
        outer.new Inner().innerPublicReflection(outer);

    }
}
