# LocalVariable Type-Inference

- Until java 9 we had to specify the type of a local variable and this type should have to be compatible with the init value.
- From java 10 we are allowed not to provide the type of a local variable explicitly. If we ommit the type to compiler infers
it from the type of the initializer.
- This feature available only for local variables.
- The initializer is required. 

Flight recorder
```bash
java -XX:StartFlightRecording=duration=10s,settings=profile,filename=java-demo-app.jfr -jar api-demo-1.0-SNAPSHOT.jar 

```
    