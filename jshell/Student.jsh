public class Student extends Person {
	private String id;
	private int credits;


    public Student() {
        super();
        id = "$xxx";
        credits = 0;
    }

	public Student(String name, int age, String id, int credits) {
		super(name, age);
		this.id = id;
		this.credits = credits;
	}

    public void setID(String id) {
        this.id = id;
    }

	public String getID() {
		return id;
	}

	public void setCredits(int credits) {
        this.credits = credits;
    }
    
    public int getCredits() {
		return credits;
	}

	public void incrementCredits(int credits) {
		this.credits += credits;
	}

	@Override 
	public String toString() {
	   return super.toString() + String.format(" ID: %s, Credits: %d", id, credits);
	}
}
