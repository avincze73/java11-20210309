# Basic shell commands
```bash
1 + 2

System.out.println("Hello world")

if (10 < 20) System.out.println("True"); else System.out.println("False");

for (int i=0; i<5; i++) System.out.println(i);

Math.PI
Math.random()
Math.random()

Math.pow(2,8)
Math.PI * 5*5

System.out.printf("My name is %s and I am %d years old\n","Frank", 22)

List<String> names = List.of("Jack", "Jill", "Mike");
names
names.forEach(System.out::println)
names.contains("Jack")
names.contains("Jill")
names.size()
names.toArray()

String line = "";
BufferedReader reader = new BufferedReader(new FileReader("file.txt"));
while ((line = reader.readLine()) != null) System.out.println(line);
reader.close()
```


# Getting help
```bash
/exit
jshell --help
jshell --version
# intro doc
/help intro

#Clears the jshell screen
ctrl+l

#List of commands
/help
/?
/help list
/help vars

/TAB
/list - TAB
```

# Understanding snippets
```bash
#clears the histroy
/reset
500 * 2
int x = 10
int y = 1000
double pi = Math.PI
$1 == $2
int name = "Mike"
String name = "Mike"
System.out.println("hello")
System.out.println(y)
void sayHello(String s) { System.out.println("Hello " + s); }
sayHello("Everyone!")
$1 == $2
$1
$2
$1 = 1000
$1 == $2
#all valid code snippets
/list
/list 1 2
/list x
/list sayHello

#Gives explicit, implicit, error snippets
/list -all

/drop snippetID
```



# Editing and navigating code snippets
```bash
#clears the histroy
/reset

int a = 10
int b = 100
String s = "Mike"
/list
System.out.println(s)
/list
/history
System.out.println("Hello " + s)
/history
// CTRL-A and CTRL-E
//META-B and META-F
// Backspace and Delete
// CTRL-K
// CTRL-W
// CTRL -Y
/history
// CTRL - R searching for a string
String s = "Mike"
```


# Variables
```bash
#clears the histroy
/reset

int x = 100
//int is the implicit type 
100 + 300
int a = 10
int b = 20
int c = a + b
//String is the implicit type 
"Mike"
String mike = "Mike"
/list
/vars
String x = "New Value"
/vars
/var -all
/drop $2
/vars
/vars -all
List<String> colors = List.of("Red","Green","Blue")
/vars
colors
colors.get(0)
/list
/vars
List<String> cars = List.of("Ford", "Peugeot", "Opel")
List<List<String>> carsAndColors = List.of(cars, colors)
carsAndColors
carsAndColors.get(0)
carsAndColors.get(0).get(0)
List<String> friends = new Arraylist<>()
List<String> friends = new ArrayList<>()
friends.add("Jill")
friends.add("Charlie")
friends.remove("Jill")
friends
friends.remove("Joe")
/list
/var
/drop $18
/drop mike
/vars
/drop $19 $20 $22
/vars
/vars -all
System.out.println("Hello")
System.out.printf("Hello %s\n", "Frank")
$24.printf("and Hello again\n")
$24.printf("and Hello again\n").printf("XXXXXXX\n")
/vars
$24 == $25
/save run.jsh
/save -history history.jsh

```


# Methods
```bash
#clears the histroy
/reset

void sayHello() { System.out.println("hello"); }
sayHello()
void sayHello(String s) { System.out.printf("hello %s\n", s); }
/list
sayHello("Everyone!")
void method1() {}
void method2(int a) {}
void method3(int a, int b) {}
/list
void method1(int a) {}
void method1(int a, int b) {}
/list
/list method1
void method1(String s) { System.out.println(s); }
method1("Mike")
/list
/drop 5
/list
/methods
#method with multiple lines
int numChars(String s, char c) {
   int count = 0;
   for (int i=0; i<s.length(); i++)
     if (s.charAt(i) == c)
        count++;
   return count;
}
numChars("baseball", 'l')
numChars("baseball", 'b')
numChars("baseball", 'B')
numChars("baseball", 'z')
int calculateSum(int... nums) {
  int sum = 0;
  for (int i=0; i<nums.length; i++)
    sum += nums[i];
  return sum;
}
/methods
/list calculateSum
calculateSum(1,2,3,4,5)
calculateSum(100,200,300)
<T> List<T> combine(T t1, T t2) {
   List<T> l = new ArrayList<>();
   l.add(t1);
   l.add(t2);
   return l;
}
/list combine
combine(1,2)
combine("mike", "jill")
combine(List.of(1,2,3,4,5), List.of(6,7,8,9,10))
$23
/methods
/list combine
/list
double calcVolCylinder(double r, double h) {
   return Math.PI * r * r * h;
}
calcVolCylinder(10,5)
/list calcVolCylinder
double calcVolCylinder(double r, double h) {
   return calcAreaCircle(r)  * h;
}
calcVolCylinder(10,5)
double calcAreaCircle(double r) {
   return Math.PI * r * r;
}
calcVolCylinder(10,5)
calcAreaCircle(5)
/list calcVolCyliner
/list calcVolCylinder

```

# External editor
```bash
#It brings up the default built-in swing editor
/edit
/edit snippetId, var name

/help set editor

/set editor vim
/set editor "/Applications/Visual\ Studio\ Code.app"
/set editor -retain vim
/set editor -default
```

# Classes
```bash
public class Person {
    private String name;
    private int age;

    public static void method() {
        System.out.println("Static method");
    }

    public Person() {
        name = "";
        age = 0;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() { return name; }
    public int getage() { return age; }

    @Override 
    public String toString() {
        return String.format("Name: %s, Age: %d", name, age);
    }
}

public class Student extends Person {
	private String id;
	private int credits;

	public Student(String name, int age, String id, int credits) {
		super(name, age);
		this.id = id;
		this.credits = credits;
	}

	public String getID() {
		return id;
	}

	public int getCredits() {
		return credits;
	}

	public void incrementCredits(int credits) {
		this.credits += credits;
	}

	@Override 
	public String toString() {
	   return super.toString() + String.format(" ID: %s, Credits: %d", id, credits);
	}
}




#creating classes
/set editor vim
/edit
/list
/types
Person p1 = new Person()
p.getName()
p1.getName()
p.setName("Mike")
p1.setName("Mike")
p1.getName()
p1.name
/edit Person
p1
/list Person
/edit Person
p1 = new Person()
p1.setName("Joe")
p1.setAge(18)
p1.getAge()
p1.getName()
/edit Persom
/edit Person
Person p2 = new Person("Angie", 30)
/edit Person
/list Person Student
/vars
p1 = new Person("Bill", 65)
/vars
Student dan = new Student("Dan",19,"S1234", 60)
/types
Person s1 = new Person("Maggie", 19)
Person s2 = new Person("Mitch", 29)
Person s3 = new Student("Michael",17, "$12345", 70)
s1.getName()
s3.getName()
s1.getID()
s3.getID()
/vars
List<Person> persons = List.of(s1,s2,s3)
persons.forEach(p->System.out.println(p.getName())
)

```


# Starts
```bash
#startup environment
jshell -v --startup PRINTING DEFAULT myStartup.jsh
/vars
/list -all
print("hello")
/help shortcuts
```

# Saving and loading
```bash
/open Person.jsh
/types
/list
/open Student.jsh
/types
Person p = new Person("Frank", 18)
Student q = new Student("Joe", 28, "$1234", 100)
/vars
/help save
/save active.jsh
int i = "Frank"
/save -all all.jsh
/save -history
/save -history history.jsh
/save -start start.jsh
```


# Jars and modules
```bash
#aaa.jar and bbb.jar are on the class-path
jshell -v --class-path jars/aaa.jar:jars/bbb.jar

import hu.sprint.mypackage.*
/imports
/env --class-path jars/aaa.jar:jars/bbb.jar

/env --module-path mods/ccc.jar
/env --add-modules hu.sprint.mymodule


```