public class Person {
    private String name;
    private int age;

    public static void method() {
        System.out.println("Static method");
    }

    public Person() {
        name = "";
        age = 0;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
   
    public void setName(String name) { 
        this.name = name; 
    }

    public String getName() { 
        return name; 
    }

    public void setAge(int age) { 
        this.age = age; 
    }

    public int getage() { 
        return age; 
    }

    @Override 
    public String toString() {
        return String.format("Name: %s, Age: %d", name, age);
    }
}
