package hu.sprint;

import java.util.concurrent.Flow;

public class EmployeeSubscriber implements Flow.Subscriber<Employee> {

    private Flow.Subscription subscription;
    private int counter = 0;


    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        System.out.println("onSubscribe for EmployeeSubscriber called");
        this.subscription = subscription;
        this.subscription.request(1); //requesting data from publisher
        System.out.println("onSubscribe for EmployeeSubscriber requested 1 employee");

    }

    @Override
    public void onNext(Employee item) {
        System.out.println("Processing Student " + item);
        counter++;
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("Some error happened");
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("All Processing Done");
    }

    public int getCounter() {
        return counter;
    }
}
