package hu.sprint;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class EmployeeRepository {
    private List<Employee> employeeList;

    public EmployeeRepository() {
        employeeList = new ArrayList<>();
        employeeList.add(Employee.builder().id(1).title("title1").fullName("fullName1").
                loginName("loginName1").password("password1").build());
        employeeList.add(Employee.builder().id(2).title("title2").fullName("fullName2").
                loginName("loginName2").password("password2").build());
        employeeList.add(Employee.builder().id(3).title("title3").fullName("fullName3").
                loginName("loginName3").password("password3").build());
    }
}
