package hu.sprint;

import lombok.*;

@Builder
@Getter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Employee {
    private int id;
    private String title;
    private String fullName;
    private String loginName;
    private String password;
}
