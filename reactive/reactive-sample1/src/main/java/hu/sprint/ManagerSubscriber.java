package hu.sprint;

import java.util.concurrent.Flow;

public class ManagerSubscriber implements Flow.Subscriber<Manager> {

    private Flow.Subscription subscription;
    private int counter = 0;


    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        System.out.println("onSubscribe for ManagerSubscriber called");
        this.subscription = subscription;
        this.subscription.request(1); //requesting data from publisher
        System.out.println("onSubscribe for ManagerSubscriber requested 1 manager");

    }

    @Override
    public void onNext(Manager item) {
        System.out.println("Processing Manager " + item);
        counter++;
        if(counter==2) {
            this.subscription.cancel();
            return;
        }
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("Some error happened");
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("All Processing Done");
    }

    public int getCounter() {
        return counter;
    }
}
