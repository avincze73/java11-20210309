package hu.sprint;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Manager extends Employee{
    private int managerId;

    public Manager(Employee employee){
        super(employee.getId(), employee.getTitle(), employee.getFullName(), employee.getLoginName(), employee.getPassword());
        this.managerId = employee.getId() + 100;
    }

}
