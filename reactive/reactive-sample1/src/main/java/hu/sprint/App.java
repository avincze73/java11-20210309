package hu.sprint;

import java.util.List;
import java.util.concurrent.SubmissionPublisher;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        // Create Publisher
        SubmissionPublisher<Employee> publisher = new SubmissionPublisher<>();

        // Register Subscriber
        EmployeeSubscriber subs = new EmployeeSubscriber();
        publisher.subscribe(subs);

        List<Employee> students = new EmployeeRepository().getEmployeeList();

        // Publish items
        System.out.println("Publishing Items to Subscriber");
        students.forEach(publisher::submit);

        // logic to wait till processing of all messages are over
        while (students.size() != subs.getCounter()) {
            Thread.sleep(1000);
        }
        // close the Publisher
        publisher.close();

        System.out.println("Exiting the app");

    }
}
