package hu.sprint;

import java.util.List;
import java.util.concurrent.SubmissionPublisher;

public class App2 {

    public static void main(String[] args) throws InterruptedException {
        // Create End Publisher
        SubmissionPublisher<Employee> publisher = new SubmissionPublisher<>();

        // Create Processor
        CustomProcessor transformProcessor = new CustomProcessor(student ->
                new Manager(student));

        //Create End Subscriber
        ManagerSubscriber subs = new ManagerSubscriber();

        //Create chain of publisher, processor and subscriber
        publisher.subscribe(transformProcessor); // publisher to processor
        transformProcessor.subscribe(subs); // processor to subscriber

        List<Employee> emps = new EmployeeRepository().getEmployeeList();

        // Publish items
        System.out.println("Publishing Items to Subscriber");
        emps.forEach(publisher::submit);

        // Logic to wait for messages processing to finish
        while (emps.size() != subs.getCounter() +1) {
            Thread.sleep(10);
        }

        // Closing publishers
        publisher.close();
        transformProcessor.close();

        System.out.println("Exiting the app");
    }

}
