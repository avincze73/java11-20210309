# Garbage collector

# Overview

- During GC objects in the heap are collected.
- GC events contain three phases: marking, deletion, copying/compaction.
- In the first phase GC goes through the heap and marks everything as referenced, unreferenced objects or available memory space.
- In the second phase Unreferenced objects are deleted.
- In the third phase Referenced objects are compacted or moved to specific areas.
- In generational collections, objects age which means they are promoted through 3 spaces in their lives (Eden, Survivor, Tenured).
- This shift occurs during the compaction phase.
  ![Old way of gc](../gc/old-gc-heap.png "Old way of gc")
- Developers do not need to know how does GC work.
- Understanding GC is somewhat like knowing linux cli command. You do not really need to use them but it helps you to see the hidden processes.
- Knowing that GC affects the application performance is basic.
- Sometimes GC is called a Stop The World event, because all threads are stopped to allow GC to run. If this is a short time
  it does not have influence on the running application. If this Stop The World event takes 2 seconds to execute then the
  end user experiences 2 seconds delay, which is bad.
- GC is a computationally heavy operation so CPU usage of it can be intensive.

# General GC

- Before we dive into the different kind of GCs and their performance impact it is important to understand the basics of generational
  GC.
- The longer a reference exists to an object in the heap, the less likely it is to be marked for deletion.
- The objects can be separated into different storage spaces to be marked by the GC less frequently.
- When an object is allocated to the heap it is placed to the Eden space. That is where the objects start out and in most cases
  where they are marked for deletion.
- Objects that survive this stage are copied to the Survivor space.
- The Eden and the Survivor spaces are called Young Generations.
- When an object in the Young Generation reaches a certain age it is promoted to the Tenured or Old space.
- A Minor GC is a collection that focuses only on the Young Generation. The majority of Young Generation is marked for deletion.
- Full GC includes the old generation and it is triggered only if it is necessary.

# Collector types

## Throughput collectors

- Throughput is one of the key metrics in performance testing. It's used to check how many requests a software will be able to process per second, per minute or hour.
- Throughput indicates the number of transactions per second an application can handle, the amount of transactions produced over time during a test.
- Optimized for high-throughput with higher latency.
- **Serial GC**. It is designed for single-threaded environment (32-bit or Win) and for small heaps. It requires several Full GC to
  release the unused heap space. This causes many Stop The World pauses.
- **Parallel GC**. GC uses multiple threads running in parallel to scan and compact the heap. It pauses all application threads
  while running. It is best suited for applications that need to be optimized for best throughput and can tolarate higher latency

## Low pause collectors

- Application requires low pause GC so user experience is not affected by long pauses.
  The goal of these GSs are the optimization for responsiveness and short-term pause.
- **Concurrent Mark Sweep**. It uses multiple threads to mark and sweep (remove) the unreferenced objects. It initiates
  Stop The World event only in two instances. 1. When initializing the marking of root objects in the old generation. 2. When algorithm
  changes the state of the heap while the GC is running.
- **G1**. Garbage First collector uses multiple threads to scan through the heap that is divided into regions. It scans those regions
  that contain the most garbage objects. This strategy reduces the change of the heap being depleted before the GC threads
  have finished their work for scanning unused objects. In this case the Stop The World event occured. G1 collector compacts the heap
  on-the-go. In case of CMS this process occured during the Stop The World event.

## Comparing the two GC algorithms

- Parallel GC is optimized for throughput at the expense of longer GC pauses
- G1 GC is optimized for shorter GC pauses at the expense of lower throughput.

## Improving the GC performance

- Increasing the heap size would decrease the GC frequency with increased duration and decreasing heap size with decreasing the GC duration while increasing the frequency.

# Why Does the Choice of Garbage Collector Matter?

The purpose of a garbage collector is to free the application developer from manual dynamic memory management.

When does the choice of a garbage collector matter?
It's very important to keep the overhead of doing garbage collection as low as possible.

**Throughput Goal**
The throughput goal is measured in terms of the time spent collecting garbage, and the time spent outside of garbage collection which is the application time.
The goal is specified by the command-line option **-XX:GCTimeRatio=nnn**. The ratio of garbage collection time to application time is 1/ (1+nnn). For example, **-XX:GCTimeRatio=19** sets a goal of 1/20th or 5% of the total time for garbage collection.

**Maximum Pause-Time Goal**
The pause time is the duration during which the garbage collector stops the application and recovers space that's no longer in use. The intent of the maximum pause-time goal is to limit the longest of these pauses.

**Footprint**
If the throughput and maximum pause-time goals have been met, then the garbage collector reduces the size of the heap until one of the goals (invariably the throughput goal) can't be met. The minimum and maximum heap sizes that the garbage collector can use can be set using **-Xms=<nnn>** and **-Xmx=<mmm>** for minimum and maximum heap size respectively.

An object is considered garbage and its memory can be reused by the VM when it can no longer be reached from any reference of any other live object in the running program.

Generational Garbage Collection

- An object is considered garbage when it can no longer be reached from any reference.
- A most straightforward garbage collection algorithm iterates over every reachable object every time it runs.
- Any leftover objects are considered garbage. The time this approach takes is proportional to the number of live objects
- JVM incorporates a number of different garbage collection algorithms that all use a technique called generational collection.
- Naive garbage collection examines every live object in the heap every time.
- Generational collection exploits several empirically observed properties of most applications to minimize the work required to reclaim unused (garbage) objects.
- Weak generational hypothesis, which states that most objects survive for only a short period of time.

Generations

- Memory is managed in generations (memory pools holding objects of different ages)
- Garbage collection occurs in each generation
- The vast majority of objects are allocated in a pool dedicated to young objects (the young generation), and most objects die there.
- When the young generation fills up, it causes a **minor** collection in which only the young generation is collected
- The costs of such collections are proportional to the number of live objects being collected.
- Some fraction of the surviving objects from the young generation are moved to the old generation during each minor collection.
- The old generation fills up and must be collected, resulting in a **major collection**, in which the entire heap is collected.

Performance Considerations

- **Throughput** is the percentage of total time **not spent** in garbage collection considered over long periods of time. Throughput includes time spent in allocation.
- **Latency** is the responsiveness of an application. Garbage collection pauses affect the responsiveness of applications.
- **Footprint** is the working set of a process, measured in pages and cache lines.
- **Promptness** is the time between when an object becomes dead and when the memory becomes available.

Use cases

- Some considers the right metric for a web server to be throughput because pauses during garbage collection may be tolerable or simply obscured by network latencies.
- Interactive graphics program, even short pauses may negatively affect the user experience.
- A very large young generation may maximize throughput, but does so at the expense of footprint, promptness, and pause times.
- Young generation pauses can be minimized by using a small young generation at the expense of throughput.

> There is no one right way to choose the size of a generation.
> The best choice is determined by the way the application uses memory as well as user requirements.

## Throughput and Footprint Measurement

Pauses due to garbage collection are easily estimated by inspecting the diagnostic output of the virtual machine itself.

```bash
[15,651s][info ][gc] GC(36) Pause Young (G1 Evacuation Pause) 239M->57M(307M) (15,646s, 15,651s) 5,048ms
[16,162s][info ][gc] GC(37) Pause Young (G1 Evacuation Pause) 238M->57M(307M) (16,146s, 16,162s) 16,565ms
[16,367s][info ][gc] GC(38) Pause Full (System.gc()) 69M->31M(104M) (16,202s, 16,367s) 164,581ms
```

```bash
java -Xlog:gc -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/jfr-demo/target/jfr-demo-0.0.1-SNAPSHOT.jar
java "-Xlog:gc*" -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/jfr-demo/target/jfr-demo-0.0.1-SNAPSHOT.jar
```

## Factors affecting gargabe collection performance

### Total heap

- Because collections occur when generations fill up, throughput is inversely proportional to the amount of memory available.

### Heap options affecting generation size

![Heap options](https://docs.oracle.com/en/java/javase/11/gctuning/img/jsgct_dt_006_prm_gn_sz_new.png)

-

## Available Collectors

### Serial Collector

- It uses a single thread to perform all garbage collection work, which makes it relatively efficient because there is no communication overhead between threads.
- It's best-suited to single processor machines because it can't take advantage of multiprocessor hardware, although it can be useful on multiprocessors for applications with small data sets (up to approximately 100 MB).
- It can be explicitly enabled with the option **-XX:+UseSerialGC**.

### Parralel Collector

- It is also known as throughput collector.
- It has multiple threads that are used to speed up garbage collection.
- It is intended for applications with medium-sized to large-sized data sets that are run on multiprocessor or multithreaded hardware.
- It can be explicitly enabled with the option **-XX:+UseParallelGC**.
- Parallel compaction is a feature that enables the parallel collector to perform major collections in parallel. It can be disabled by using the **-XX:-UseParallelOldGC** option.

### Mostly Concurrent Collectors

Concurrent Mark Sweep (CMS) collector and Garbage-First (G1) garbage collector are the two mostly concurrent collectors.

- CMS collector: This collector is for applications that prefer shorter garbage collection pauses and can afford to share processor resources with the garbage collection. Use the option __-XX:+UseConcMarkSweepGC__  to enable the CMS collector
- G1 garbage collector: This server-style collector is for multiprocessor machines with a large amount of memory. It meets garbage collection pause-time goals with high probability, while achieving high throughput.
  G1 is selected by default on certain hardware and operating system configurations, or can be explicitly enabled using __-XX:+UseG1GC__ .

### Z Garbage Collector (ZGC)

- It is a scalable low latency garbage collector.
- It performs all expensive work concurrently, without stopping the execution of application threads.
- ZGC is intended for applications which require low latency (less than 10 ms pauses) and/or use a very large heap (multi-terabytes).
- You can enable is by using the __-XX:+UseZGC__ option.

### Selecting a Collector

- At first run your application and allow the VM to select a collector.
- Adjust the heap size to improve performance.
- If the application has a small data set (up to approximately 100 MB), then select the serial collector with the option __-XX:+UseSerialGC__.
- If the application will be run on a single processor and there are no pause-time requirements, then select the serial collector with the option __-XX:+UseSerialGC__.
- If the application performance is the first priority and there are no pause-time requirements or pauses of one second or longer are acceptable, then let the VM select the collector or select the parallel collector with __-XX:+UseParallelGC__.
- If response time is more important than overall throughput and garbage collection pauses must be kept shorter than approximately one second, then select a mostly concurrent collector with __-XX:+UseG1GC__ or __-XX:+UseConcMarkSweepGC__.
- If response time is a high priority, and/or you are using a very large heap, then select a fully concurrent collector with __-XX:UseZGC__.

## Parallel collector

- The primary difference between the serial and parallel collectors is that the parallel collector has multiple threads that are used to speed up garbage collection.
- __-XX:+UseParallelGC__
- Both minor and major collections are run in parallel to further reduce garbage collection overhead.
- On a host with one processor, the parallel collector will likely not perform as well as the serial collector because of the overhead required for parallel execution.
- The number of garbage collector threads can be controlled with the command-line option __-XX:ParallelGCThreads=N__.
- Enabling the parallel collector should make the collection pauses shorter.


## Mostly concurrent collectors

- The mostly concurrent collectors perform parts of their work concurrently to the application.
- Concurrent Mark Sweep (CMS) collector: This collector is for applications that prefer shorter garbage collection pauses and can afford to share processor resources with the garbage collection.
- Garbage-First (G1) garbage collector: This server-style collector is for multiprocessor machines with a large amount of memory. It meets garbage collection pause-time goals with high probability while achieving high throughput.

### Concurrent Mark Sweep (CMS) Collector

- The Concurrent Mark Sweep (CMS) collector is designed for applications that prefer shorter garbage collection pauses and that can afford to share processor resources with the garbage collector while the application is running.
- Typically applications that have a relatively large set of long-lived data (a large old generation) and run on machines with two or more processors tend to benefit from the use of this collector.
- __-XX:+UseConcMarkSweepGC__
- The CMS collector is deprecated.
- If the CMS collector is unable to finish reclaiming the unreachable objects before the old generation fills up then the application is paused and the collection is completed with all the application threads stopped.
- The inability to complete a collection concurrently is referred to as concurrent mode failure and indicates the need to adjust the CMS collector parameters.
- The CMS collector throws an OutOfMemoryError if too much time is being spent in garbage collection. If more than 98% of the total time is spent in garbage collection and less than 2% of the heap is recovered, then an OutOfMemoryError is thrown.
- This feature can be disabled by adding the option __-XX:-UseGCOverheadLimit__
- Floating garbage. Floating garbage in the heap at the end of one concurrent collection cycle is collected during the next collection cycle.
- The CMS collector pauses an application twice during a concurrent collection cycle. The first pause is referred to as the initial mark pause.
  The second pause is referred to as the remark pause.
- With the serial collector a major collection occurs whenever the old generation becomes full and all application threads are stopped while the collection is done.  
  In contrast, the start of a concurrent collection in CMS collector must be timed such that the collection can finish before the old generation becomes full.
  -XX:CMSInitiatingOccupancyFraction=N where N is the percentage of initiating occupancy threshold. The default value is about 92.

### Garbage-first garbage collector

- The G1GC is targeted for multiprocessor machines with large amount of memory.
- It attempts to achieve pause-time goal with high throughput with little need for configuration.
- G1GC replaces the CMS GC.
- -XX:+UseG1GC

#### Application features work well with G1GC

- Heap sizes up to ten of GBs or larger, with more than 50% of the Java heap occupied with live data.
- A significant amount of fragmentation in the heap.
- Predictable pause-time target goals that aren’t longer than a few hundred milliseconds, avoiding long garbage collection pauses.

#### Basic concepts

- G1 is a generational, incremental, parallel, mostly concurrent, stop-the-world, and evacuating garbage collector which monitors pause-time goals in each of the stop-the-world pauses
- G1 splits the heap into (virtual) young and old generations.
- Space-reclamation efforts concentrate on the young generation where it is most efficient to do so, with occasional space-reclamation in the old generation
- G1 reclaims space mostly by using evacuation: live objects found within selected memory areas to collect are copied into new memory areas, compacting them in the process. After an evacuation has been completed, the space previously occupied by live objects is reused for allocation by the application.

#### Heap layout

- G1 partitions the heap into a set of equally sized heap region
  ![G1 Garbage Collector Heap Layout](https://docs.oracle.com/en/java/javase/11/gctuning/img/jsgct_dt_004_grbg_frst_hp.png)
- At any given time, each of these regions can be empty (light gray), or assigned to a particular generation, young or old.
- These regions are typically laid out in a noncontiguous pattern in memory.
- The young generation contains eden regions (red) and survivor regions (red with "S").
- Old regions (light blue) make up the old generation. Old generation regions may be humongous (light blue with "H") for objects that span multiple regions.
- An application always allocates into a young generation, that is, eden regions, with the exception of humongous objects that are directly allocated as belonging to the old generation.
- G1 garbage collection pauses can reclaim space in the young generation as a whole, and any additional set of old generation
  regions at any collection pause. During the pause G1 copies objects from this collection set to one or more different regions
  in the heap. The destination region for an object depends on the source region of that object: the entire young generation
  is copied into either survivor or old regions, and objects from old regions to other, different old regions using aging.

#### Garbage collection cycle

#### Garbage-First Internals

#### Garbage-First Tuning

- The general recommendation is to use G1 with its default settings, or giving it a different pause-time goal and setting a maximum Java heap size by using -Xmx if desired.
- G1's goals in the default configuration are neither maximum throughput nor lowest latency, but to provide relatively small, uniform pauses at high throughput.
- If you prefer high throughput, then relax the pause-time goal by using __-XX:MaxGCPauseMillis__ or provide a larger heap.
- If latency is the main requirement, then modify the pause-time target.
- Avoid limiting the young generation size to particular values by using options like -Xmn, -XX:NewRatio and others because the young generation size is the main means for G1 to allow it to meet the pause-time.
- Setting the young generation size to a single value overrides and practically disables pause-time control.

#### Moving to G1 from Other Collectors

- Removing all options that affect garbage collection, and only set the pause-time goal and overall heap size by using -Xmx and optionally -Xms.
- Many options that are useful for other collectors to respond in some particular way, have either no effect at all, or even decrease throughput and the likelihood to meet the pause-time target.
- An example could be setting young generation sizes that completely prevent G1 from adjusting the young generation size to meet pause-time goals.

#### Improving G1 Performance

- G1 is designed to provide good overall performance without the need to specify additional options.
- Application-level optimizations could be more effective than trying to tune the VM to perform better.

#### Observing Full Garbage Collections

- A full heap garbage collection (Full GC) is often very time consuming.
- Full GCs caused by too high heap occupancy in the old generation can be detected by finding the words Pause Full (Allocation Failure) in the log.
- The reason that a Full GC occurs is because the application allocates too many objects that can't be reclaimed quickly enough.
- You can determine the number of regions occupied by humongous objects on the Java heap using the gc+heap=info logging.
- -XX:G1HeapRegionSize
- Increase the size of the Java heap.
- Increase the number of concurrent marking threads by setting -XX:ConcGCThreads explicitly.
- -XX:+DisableExplicitGC

#### Tuning for Latency

#### Tuning for Throughput

#### Tuning for Heap Size

## The Z Garbage Collector

- ZGC performs all expensive work concurrently, without stopping the execution of application threads for more than 10ms, which makes is suitable for applications which require low latency and/or use a very large heap (multi-terabytes).
- -XX:+UnlockExperimentalVMOptions -XX:+UseZGC
- The most important tuning option for ZGC is setting the max heap size (-Xmx)
- The second tuning option one might want to look at is setting the number of concurrent GC threads (-XX:ConcGCThreads).

## Command line switches

Enabling the G1 Collector

```bash
java -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/jfr-demo/target/jfr-demo-0.0.1-SNAPSHOT.jar
java -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/oome/target/oome-1.0-SNAPSHOT.jar
```

Key command line options

- -XX:+UseG1GC: JVM uses the G1 GC
- -XX:MaxGCPauseMillis=200: Maximum value of GC pause time in milliseconds
- -XX:InitiatingHeapOccupancyPercent=45: Percentage of the heap occupancy to start the GC cycle.

## Best practices

- Do not set young generation size with -Xmn. Setting the generation size disables the pause time goal.

| Option and Default Value             | Description                                                                             |
| ------------------------------------ | --------------------------------------------------------------------------------------- |
| -XX:+UseG1GC                         | Use G1 GC                                                                               |
| -XX:MaxGCPauseMillis=n               | Maximum GC pause time                                                                   |
| -XX:InitiatingHeapOccupancyPercent=n | Percentage of the heap occupancy to start the GC cycle.                                 |
| -XX:NewRatio=n                       | Ratio of new/old generation sizes. The default value is 2.                              |
| -XX:SurvivorRatio=n                  | Ratio of eden/survivor space size. The default value is 8.                              |
| -XX:ParallelGCThreads=n              | Number of threads used during parallel phases of the GC.                                |
| -XX:ConcGCThreads=n                  | Number of threads concurrent GC will use.                                               |
| -XX:ConcGCThreads=n                  | This is the size of individual subdivision. The default values is between 1MB and 32MB. |

## Logging options

- -XX:+PrintGC:

```bash
java -XX:+PrintGC -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/jfr-demo/target/jfr-demo-0.0.1-SNAPSHOT.jar
java -XX:+PrintGC -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/oome/target/oome-1.0-SNAPSHOT.jar

# setting the detail level to finer
java -XX:+PrintGCDetails -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ../jfr/oome/target/oome-1.0-SNAPSHOT.jar
```
