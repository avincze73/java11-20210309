# Compiling
```bash
# Show what is the compiler output if module-info file is empty
javac --module-path mods --module-source-path src -d out $(find src -name '*.java')

jar --create --file mods/models.jar -C out/models .

jar --create --file mods/main.jar -C out/main .
```

# Running
```bash
java  --module-path mods --add-modules java.sql --module main/hu.sprint.main.Main 
```

# Examining
```bash
jar --describe-module --file mods/main.jar
jar --describe-module --file mods/models.jar
jar --describe-module --file mods/gson-2.8.1.jar
```

![Part 3.](./image/part3.png)