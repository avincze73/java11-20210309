# Compiling
```bash
javac --module-path mods -d out $(find src -name '*.java')

javac --module-path mods --add-modules gson -d out $(find src -name '*.java')

jar --create --file lib/person.jar 	-C out .
```

# Running
```bash
java --module-path mods --class-path lib/person.jar hu.sprint.main.Main

java --module-path mods --class-path lib/person.jar --add-modules gson hu.sprint.main.Main
	
```
# Examining
```bash
jdeps  -summary --module-path mods  lib/person.jar

jdeps  --add-modules gson  -summary --module-path mods  lib/person.jar
```

![Part 1.](./image/part2.png)