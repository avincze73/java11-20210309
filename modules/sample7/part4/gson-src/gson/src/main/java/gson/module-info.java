module gson {
    requires transitive java.sql;

    exports com.google.gson;
    exports com.google.gson.annotations;
    exports com.google.gson.reflect;
    exports com.google.gson.stream;

}
