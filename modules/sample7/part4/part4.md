# Generating module-info.java into gson source
```bash
jdeps --generate-module-info gson-src/gson/src/main/java gson-2.8.1.jar 
```


# Building proper gson library with module
```bash
javac --source-path gson-src/gson/src/main/java/ -d out/gson $(find gson-src/gson/src/main/java -name '*.java')

jar --create --file mods/gson.jar -C out/gson .

jar --describe-module --file mods/gson.jar


javac --module-path mods --module-source-path src -d out $(find src -name '*.java')	
	 

jar --create --file mods/models.jar -C out/models .

jar --create --file mods/main.jar -C out/main .

java --module-path mods  --module main/hu.sprint.main.Main

#Dot is for visualization
jdeps --module-path mods -recursive --dot-output dots mods/main.jar 
 
/usr/local/Cellar/graphviz/2.46.1/bin/dot dots/summary.dot 
```


# Compile the source code
```bash
javac --module-path mods --module-source-path src -d out $(find src -name '*.java')
```

# Build two modular jar files
```bash
jar --create --file mods/models.jar -C out/models .

jar --create --file mods/main.jar -C out/main .
```

# Run the app
```bash
java  --module-path mods  --module main/hu.sprint.main.Main 
java  --module-path mods  --add-modules java.sql  --module main/hu.sprint.main.Main 
```

# Describing jar files
```bash
jar --describe-module --file mods/main.jar
jar --describe-module --file mods/models.jar
jar --describe-module --file mods/gson-2.8.1.jar
```

![Part 4.](./image/part4.png)


#Webgraphwiz
http://www.webgraphviz.com


