module models {
	requires gson;
	exports hu.sprint.models;
	opens hu.sprint.models
		to gson;
}
