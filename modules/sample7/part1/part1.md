# Examining
```bash
#Does it use any jdk internal library?
jdeps --jdk-internals lib/gson-2.8.1.jar
jdeps -summary lib/gson-2.8.1.jar
```
![Part 1.](./image/internal.png)

# Compiling
```bash
javac --class-path lib/gson-2.8.1.jar -d out $(find src -name '*.java')
jar --create --file lib/person.jar -C out .
```

# Running
```bash
java --class-path lib/gson-2.8.1.jar:lib/person.jar hu.sprint.main.Main
```

# Examining
```bash
jdeps --jdk-internals lib/person.jar
jdeps -summary lib/person.jar
jdeps  lib/person.jar
jdeps -summary -cp lib/gson-2.8.1.jar lib/person.jar

```

![Part 1.](./image/part1.png)