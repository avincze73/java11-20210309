module hu.sprint.library.mod.book {
        requires hu.sprint.library4.mod;
        requires java.sql;
        exports hu.sprint.library.mod.book;
        provides hu.sprint.library.mod.LibraryServer with hu.sprint.library.mod.book.LibraryBookServer;
        }