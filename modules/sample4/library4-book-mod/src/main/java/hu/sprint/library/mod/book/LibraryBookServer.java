package hu.sprint.library.mod.book;

import hu.sprint.library.mod.LibraryServer;
import hu.sprint.library.mod.book.internal.Filter;
import java.sql.*;

import java.util.List;

public class LibraryBookServer implements LibraryServer {
    public List<String> getItems(){
        List<String> result = List.of("Martin Fawler - Refactoring: improving the design of existing code",
                "Joshua Block - Effective Java",
                "Robert C. Martin - Clean Code");
        return new Filter().filter(result);
    }

    @Override
    public String getName() {
        return "LibraryBookServer";
    }


}
