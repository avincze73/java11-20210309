package hu.sprint.library.mod;


import java.util.List;

public interface LibraryServer {
    List<String> getItems();
    String getName();
}
