package hu.sprint.library.cli2;

import hu.sprint.library.mod.LibraryServer;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        ServiceLoader<LibraryServer> loader = ServiceLoader.load(LibraryServer.class);
        Map<String, LibraryServer> serverMap = new LinkedHashMap<>();
        int key = 1;
        for (LibraryServer l: loader) {
            serverMap.put(String.valueOf(key), l);
            key++;
        }
        serverMap.forEach((k, v) -> {
            System.out.println(v.getName());
            System.out.println(v.getItems());

        });
    }
}
