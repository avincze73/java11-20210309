module hu.sprint.library.cli {
    exports hu.sprint.library.cli;
    requires hu.sprint.library4.mod;
    requires hu.sprint.library.mod.book;
    requires hu.sprint.library.mod.film;
}