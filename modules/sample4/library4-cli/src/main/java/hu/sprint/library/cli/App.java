package hu.sprint.library.cli;

import hu.sprint.library.mod.LibraryServer;
import hu.sprint.library.mod.book.LibraryBookServer;
import hu.sprint.library.mod.film.LibraryFilmServer;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        Map<String, LibraryServer> serverMap = new LinkedHashMap<>();
        serverMap.put("Book", new LibraryBookServer());
        serverMap.put("Film", new LibraryFilmServer());

        serverMap.forEach((k, v) -> {
            System.out.println(v.getName());
            System.out.println(v.getItems());

        });
    }
}
