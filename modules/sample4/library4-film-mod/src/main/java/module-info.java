module hu.sprint.library.mod.film {
    requires hu.sprint.library4.mod;
    exports hu.sprint.library.mod.film;
    provides hu.sprint.library.mod.LibraryServer with hu.sprint.library.mod.film.LibraryFilmServer;

}