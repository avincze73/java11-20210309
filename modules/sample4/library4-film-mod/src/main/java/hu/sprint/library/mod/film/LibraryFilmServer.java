package hu.sprint.library.mod.film;

import hu.sprint.library.mod.LibraryServer;
import hu.sprint.library.mod.film.internal.Filter;

import java.util.List;

public class LibraryFilmServer implements LibraryServer {
    public List<String> getItems(){
        List<String> result = List.of("George Lucas - Star Wars IV.",
                "Steven Spielberg - Schinder's List",
                "Steven Spielberg - Raiders of the lost ark");
        return new Filter().filter(result);
    }

    @Override
    public String getName() {
        return "LibraryFilmServer";
    }
}
