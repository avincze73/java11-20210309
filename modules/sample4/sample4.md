# custom jre with jlink

```bash
jlink \
        --module-path ./mods:/Users/avincze/Documents/Library/javafx-sdk-11.0.2/lib \
        --add-modules hu.sprint.library.ui,javafx.controls,javafx.fxml \
        --launcher JOKER=hu.sprint.library.ui/hu.sprint.library.ui.Main --output ./jre
        
# it works        
rm -rf dist
jlink --module-path ./mods:/Users/avincze/Documents/Library/javafx-jmods-11.0.2 \
--add-modules hu.sprint.library.ui,hu.sprint.library.mod.book,hu.sprint.library.mod.film --launcher start=hu.sprint.library.ui/hu.sprint.library.ui.Main --output dist
dist/bin/start
        
jre/bin/java --list-modules
du -sh dist

jlink --module-path ./mods:/Users/avincze/Documents/Library/javafx-jmods-11.0.2 \
--add-modules hu.sprint.library.ui,hu.sprint.library.mod.book,hu.sprint.library.mod.film \
--launcher start=hu.sprint.library.ui/hu.sprint.library.ui.Main  \
--compress 2 --no-header-files --no-man-pages --strip-debug  --output dist

du -sh dist
```