package hu.sprint.library.ui;

import hu.sprint.library.mod.LibraryServer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;

import java.util.ServiceLoader;


public class Controller {


    private ServiceLoader<LibraryServer> loader;

    @FXML
    private TextArea jokeTextArea;

    @FXML
    private HBox buttonsArea;

    @FXML
    private void initialize() {
        System.out.println("Init in Controller");
        loader = ServiceLoader.load(LibraryServer.class);
        buttonsArea.setSpacing(10);
        loader.forEach(t-> {
            Button b = new Button(t.getName());
            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    jokeTextArea.setText(t.getItems().toString());
                }
            });
            buttonsArea.getChildren().add(b);
        });
        Button b = new Button("Clear");
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                jokeTextArea.clear();
            }
        });
        buttonsArea.getChildren().add(b);

    }


}
