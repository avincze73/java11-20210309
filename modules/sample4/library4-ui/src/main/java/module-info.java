module hu.sprint.library.ui {
    //exports hu.sprint.library.ui;
    requires javafx.base;
    requires javafx.fxml;
    requires javafx.controls;
    requires hu.sprint.library4.mod;

    uses hu.sprint.library.mod.LibraryServer;
    opens hu.sprint.library.ui to javafx.fxml, javafx.graphics;
}