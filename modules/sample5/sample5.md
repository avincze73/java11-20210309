# Nothing is on the module path
## Compile
```bash
javac --class-path lib/gson-2.8.1.jar -d out $(find src -name '*.java')

jar --create --file lib/person.jar -C out .
```
## Run
```bash
java --class-path lib/gson-2.8.1.jar:lib/person.jar hu.sprint.Main

jdeps --jdk-internals lib/gson-2.8.1.jar
# It is a self contained jar file it has anything it needs
jdeps -summary lib/gson-2.8.1.jar
jdeps -summary -cp lib/gson-2.8.1.jar  lib/person.jar
jdeps lib/person.jar
jdeps lib/gson-2.8.1.jar
jar tf lib/person.jar
```
## Everything is in the so-called unnamed module


# gson lib is on the module path
## Compile
```bash
javac --module-path mods d out $(find src -name '*.java')
javac --module-path mods --add-modules gson -d out $(find src -name '*.java')

jar --create --file lib/person.jar -C out .
```

## Run
```bash
java --module-path mods --class-path lib/person.jar --add-modules gson hu.sprint.Main
jdeps -summary --module-path mods lib/person.jar
```
# person is on the module path
