# Exercise

- Write module which can find all anagrams of a word from the dictionary.txt file.
- Use service based approach separating the service and its implementation.
- Write a client program to test the service.