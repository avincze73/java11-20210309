package hu.sprint;

import hu.sprint.bottom.BottomClass;
import hu.sprint.middle.MiddleClass;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MiddleClass middleClass = new MiddleClass();
        System.out.println(middleClass.getInfo());
        BottomClass bottomClass = new BottomClass();
        System.out.printf(bottomClass.getInfo());
    }
}
