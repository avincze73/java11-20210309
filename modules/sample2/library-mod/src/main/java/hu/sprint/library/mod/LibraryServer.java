package hu.sprint.library.mod;

import hu.sprint.library.mod.book.internal.Filter;

import java.util.List;
import java.util.logging.Logger;

public class LibraryServer {
    public List<String> getBooks(){
        Logger.getLogger(this.getClass().getName()).info("LibraryServer");
        List<String> result = List.of("Martin Fawler - Refactoring: improving the design of existing code",
                "Joshua Block - Effective Java",
                "Robert C. Martin - Clean Code");
        return new Filter().filter(result);
    }
}
