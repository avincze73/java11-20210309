package hu.sprint.library.cli;

import hu.sprint.library.mod.LibraryServer;

import java.util.List;
import java.util.logging.Logger;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Logger.getLogger("Main").info("LibraryServer");
        LibraryServer libraryServer = new LibraryServer();
        List<String> result = libraryServer.getBooks();
    }
}
