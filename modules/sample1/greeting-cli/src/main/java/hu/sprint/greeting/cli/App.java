package hu.sprint.greeting.cli;

import hu.sprint.greetings.mod.Greetings;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Greetings greetings = new Greetings();
        System.out.println(greetings.sayHello());
    }

}
