# AppCDS
What is it?
- We can use application class-data sharing to reduce launch times, response times, and memory footprint.
- The JVM's class loading workload can be reduced considerably.


When we execute a class' bytecode, the JVM needs to perform a few steps. 
- It looks the class up on disk 
- Loads it
- Verifies the bytecode
- Pulls it into an internal data structure. 
  
That takes some time of course, which is most noticeable when the JVM launches and needs to load at least a couple of hundred of classes.
- As long as the application's JARs do not change, this class-data is always the same.

What is behind AppCDS?
- Create this data once, dump it into an archive, and then reuse that in future launches and even share it across simultaneously running JVM instances.


```bash
#Create archive for default JDK classes
sudo java -Xshare:dump
#Using class-data archive
java -Xshare:on  -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App
#Checking logs
java -Xlog:class+load:file=cds.log -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App

time java -Xshare:off -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App 
time java -Xshare:on -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App


#Get the list of classes to archive
java -Xshare:off -XX:+UseAppCDS -XX:DumpLoadedClassList=hello.lst -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App
#Create the AppCDS archive
java -Xshare:dump -XX:+UseAppCDS -XX:SharedClassListFile=hello.lst -XX:SharedArchiveFile=hello.jsa -cp target/appcds-demo-1.0-SNAPSHOT.jar
#Use the AppCDS archive
java -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa  -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App
#Creating logs
java -Xlog:class+load:file=cds.log -Xshare:on -XX:+UseAppCDS -XX:SharedArchiveFile=hello.jsa  -cp target/appcds-demo-1.0-SNAPSHOT.jar hu.sprint.App
#graphviz
jdeps -recursive --dot-output dots target/appcds-demo-1.0-SNAPSHOT.jar
/usr/local/Cellar/graphviz/2.46.1/bin/dot -Tpng -O dots/summary.dot

```



- When launching with an archive, the JVM maps the archive file into its own memory and then has most classes it needs readily available
- It doesn't have to muck around with the intricate class-loading mechanism. 
- The memory region can even be shared between concurrently running JVM instances, 
  which frees up memory that would otherwise be wasted on replicating the same information in each instance.
-   

