
# Checking used jdk
```bash
#App.class is compiled with jdk8
jdeps -jdkinternals App.class
#It shows this class uses removed API
```


# Understanding Runtime Access Warnings
```bash
java -jar jython-standalone-2.7.0.jar
java --illegal-access=warn --illegal-access=debug -jar jython-standalone-2.7.0.jar
java --add-opens java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED -jar jython-standalone-2.7.0.jar 
```