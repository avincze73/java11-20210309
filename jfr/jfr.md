# Run the app
```bash
java -XX:+FlightRecorder -XX:StartFlightRecording=duration=100s,filename=flight.jfr -cp target/oome-1.0-SNAPSHOT.jar hu.sprint.App

jcmd 608 JFR.start duration=100s filename=flight2.jfr


java -Xmx50m -Xms50m  -XX:+FlightRecorder -XX:StartFlightRecording=duration=200s,filename=flight.jfr -cp ./target/oome-1.0-SNAPSHOT.jar hu.sprint.App2
java -Xmx50m -Xms50m -XX:+FlightRecorder -XX:StartFlightRecording=duration=100s,filename=flight.jfr -cp ./target/oome-1.0-SNAPSHOT.jar hu.sprint.App
java "-Xlog:gc=info,gc+phases=debug:gc-log.txt" -Xmx50m -Xms50m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ./target/oome-1.0-SNAPSHOT.jar hu.sprint.App
java "-Xlog:gc=info:gc-log.txt" -Xmx500m -Xms500m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -jar ./target/oome-1.0-SNAPSHOT.jar hu.sprint.App
```