package hu.sprint;

import java.util.ArrayList;
import java.util.List;

public class App2 {

    public static void main(String[] args) {
        List<Object> items = new ArrayList<>(1);
        try {
            while (true){
                items.add(new Object());
                Thread.sleep(100);
            }
        } catch (OutOfMemoryError | InterruptedException e){
            System.out.println(e.getMessage());
        }
        assert items.size() > 0;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
