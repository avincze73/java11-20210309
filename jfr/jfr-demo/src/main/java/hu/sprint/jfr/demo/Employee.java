package hu.sprint.jfr.demo;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Employee {
    private int id;
    private String fullName;
    private int salary;
}
