package hu.sprint.jfr.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoService {

    @GetMapping("/gc")
    public void gc(){
        System.gc();
    }

    @GetMapping("/employee")
    public Employee employee(){
        return Employee.builder().id(1).fullName("fullName").salary(1000).build();
    }
}
